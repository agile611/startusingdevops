# Start Using Devops

Here there is a working environment to start playing and learning DevOps.

### Tools you need to run the code

If you want to use the code from this repo, you will need the following tools:

* [git](https://git-scm.com)  
* [Vagrant](https://www.vagrantup.com)  
* [Virtualbox](https://www.virtualbox.org)  
* [Visual Studio Code](https://code.visualstudio.com)  (Optional)

### What you can find here

We provided different repos to start using DevOps and learning the path.

